# Temporary Suspension

## Outline

The Temporary Suspension module allows you to quickly and easily suspend users
from your site, either indefinitely, or for a pre-determined amount of time.
If the latter is chosen, then a Cron job will handle the process of reinstating
a user at the appropriate time.

## Installation

Place the folder 'tempsuspend' inside the 'sites'all'modules/' directory. From
here:

### Using Drush:

* In your terminal, run 'drush en temp_suspend -y'. This will run through the
install hooks for the module, ensuring the database table is created.
* For good measure, it is a good idea to clear the cache in order to ensure the
new menu item appears. In your terminal, run 'drush cc menu', which will clear
the Drupal menu caches.

### Using the Drupal UI

* Navigate to '/admin/build/modules' on your website, and scroll down until you
find 'Temporary Suspension'. Click the tickbox to the left of its title, then
scroll down (or up, depending which theme you're using!) and click 'save'.
* Following this/ navigate to '/admin/settings/performance' and click 'clear
caches'. This will clear the entirety of Drupal's cache.

## Usage

### Using the Drupal UI

Visit 'admin/user/suspend'. That's it!

### Using Drush

Unfortunately this has not yet been implemented. But check back soon!
