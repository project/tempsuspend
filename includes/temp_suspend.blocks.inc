<?php
/**
 * @file
 * Temporary Suspension - blocks include file.
 */

/**
 * Implements content generation for temp_suspend_block().
 */
function temp_suspend_page() {
  $sql = 'SELECT uid, user_name, suspend_until FROM {temp_suspend}';
  $limit = 10;
  $output = '';
  $header = array(
    array(
      'data' => t('Name'),
      'field' => 'name',
    ),
    array(
      'data' => t('Uid'),
      'field' => 'uid',
    ),
    array(
      'data' => t('Suspended until'),
      'field' => 'suspend_until',
      'sort' => 'asc',
    ),
  );
  $tablesort = tablesort_sql($header);
  $result = pager_query($sql . $tablesort, $limit);
  $rows = array();
  while ($user_name = db_fetch_object($result)) {
    $check = db_result(db_query('SELECT status FROM {users} WHERE uid="%d"', $user_name->uid));
    if ((int)$check === 0) {
      $link = l($user_name->user_name, 'user/' . $user_name->uid, array('attributes' => array('class' => array('user-link'))));
      $rows[] = array(
        $link,
        $user_name->uid,
        gmdate("d-m-y", $user_name->suspend_until),
      );
    }
  }
  if (!$rows) {
    $rows[] = array(array('data' => t('No user have been suspended yet.'), 'colspan' => 3));
  }
  $output .= theme('table', $header, $rows);
  $output .= theme('pager', NULL, $limit, 0);
  return $output;
}
