<?php
/**
 * @file
 * Temporary Suspension admin include.
 */

/**
 * Administration form callback.
 *
 * @return array
 *   Build array for the administration form.
 */
function temp_suspend_settings_form() {
  $form = array();

  // An array of suspension options.
  $options = array(
    TEMP_SUSPEND_FULL => t('Full Suspension'),
    TEMP_SUSPEND_TEMP => t('Temporary Suspension'),
    TEMP_SUSPEND_REVOKE => t('Reinstate User'),
  );

  $form['user_set'] = array(
    '#type' => 'fieldset',
    '#title' => t('User to suspend'),
  );

  $form['user_set']['user_to_suspend'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#size' => 20,
    '#maxlength' => 128,
    '#description' => t('Enter the username of the user you wish to suspend'),
    '#required' => TRUE,
    '#autocomplete_path' => 'user/autocomplete',
  );

  $form['suspension_set'] = array(
    '#type' => 'fieldset',
    '#title' => t('Suspension type to enforce'),
  );

  $form['suspension_set']['suspension_type'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#required' => TRUE,
    '#description' => t('Choose to fully suspend a user until further notice, temporarily
    suspend a user until a given time, or reinstate a user.'),
  );

  $form['date_set'] = array(
    '#type' => 'fieldset',
    '#title' => t('Date to suspend until'),
  );

  $form['date_set']['date_select'] = array(
    '#type' => 'date_popup',
    '#date_format' => 'd-m-y',
  );

  $form['save_values'] = array(
    '#type' => 'value',
    '#value' => array(),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  $form['#after_build'][] = 'temp_suspend_after_build';

  return $form;
}

/**
 * Verifies that the name entered is a user on the site.
 *
 * @param string $name
 *   The username to verify.
 */
function temp_suspend_verify_user($name) {
  $result = db_result(db_query('SELECT name FROM {users} WHERE name = "%s"', $name));

  if (is_null($result)) {
    form_set_error('user_to_suspend', t('This user is not registered on this site, please try again with a correct username.'));
  }
}

/**
 * Validation handler for temp_suspend_settings_form.
 *
 * Runs validation functions before the form is submitted.
 *
 * @param array $form
 *   The form build array.
 * @param array $form_state
 *   Current form state.
 */
function temp_suspend_settings_form_validate($form, &$form_state) {
  $name = $form_state['values']['user_to_suspend'];
  $sus_until = $form_state['values']['date_select'];
  $suspension = $form_state['values']['suspension_type'];

  temp_suspend_verify_user($name);

  switch ($suspension) {
    case TEMP_SUSPEND_TEMP:
      temp_suspend_date_validator($sus_until, $converted);
      break;

    default:
      return;
  }

  form_set_value($form['save_values'], $converted, $form_state);
}

/**
 * Submit handler for temp_suspend_settings_form.
 *
 * @param array $form
 *   The form build array.
 * @param array $form_state
 *   Current form state.
 */
function temp_suspend_settings_form_submit($form, &$form_state) {
  $suspension = $form_state['values']['suspension_type'];
  $name = $form_state['values']['user_to_suspend'];
  $converted = $form_state['values']['save_values'];
  temp_suspend_suspension_handler($suspension, $name, $converted);
}
