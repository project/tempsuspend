/**
 * @file
 * Temporary Suspension - Javascript to control form elements.
 */

$(document).ready(function () {
    $(".container-inline-date").parent().hide();

    $("#edit-suspension-type").change(function () {
        if ($("#edit-suspension-type").val() === "TEMP_SUSPEND_TEMP") {
            $(".container-inline-date").parent().show();
        } else {
            $(".container-inline-date").parent().hide();
        }
    });
});
